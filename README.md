# open_office_convert

#### 介绍
使用OpenOffice将Word,PPT,Excel转换为PDF

##### 说明
本项目需要依赖OpenOffice需自行下载，下载地址如下：  
[Windown版本](https://excellmedia.dl.sourceforge.net/project/openofficeorg.mirror/4.1.7/binaries/zh-CN/Apache_OpenOffice_4.1.7_Win_x86_install_zh-CN.exe)  
[Linux版本](https://excellmedia.dl.sourceforge.net/project/openofficeorg.mirror/4.1.7/binaries/zh-CN/Apache_OpenOffice_4.1.7_Linux_x86-64_install-rpm_zh-CN.tar.gz)  